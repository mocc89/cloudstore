package store.viomi.com.system.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.SalesProfileAdapter;
import store.viomi.com.system.base.BaseFragment;
import store.viomi.com.system.bean.ProfileBean;
import store.viomi.com.system.constants.HintText;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.other.LebalSet;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.utils.ToastUtil;
import store.viomi.com.system.widget.PinnedSectionListView;

/**
 * Created by viomi on 2016/11/11.
 */

@ContentView(R.layout.a_sales_profile_fragment)
public class ASalesProfileFragment extends BaseFragment {

    @ViewInject(R.id.sale_fg_loading)
    private RelativeLayout sale_fg_loading;
    @ViewInject(R.id.pslistView)
    private PinnedSectionListView pslistView;

    private String typeNO;
    private boolean isVisible;
    private boolean isPrepared;
    private boolean hasload;

    private boolean isdestroy = false;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case 0:
                    if (!isdestroy) {
                        loadingfinish();
                        hasload = true;
                        String result1 = (String) msg.obj;
                        LogUtil.mlog("oook1", result1);
                        parseJson(result1);
                    }
                    break;
                case 1:
                    loadingfinish();
                    ToastUtil.show(HintText.FAIL_REQ);
                    break;
            }
        }
    };


    public static Fragment getInstance(String type) {
        ASalesProfileFragment fragment = new ASalesProfileFragment();
        Bundle args = new Bundle();
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected void init() {

        Bundle arguments = getArguments();
        if (arguments != null) {
            typeNO = arguments.getString("type", "null");
        }
        isPrepared = true;
        if (isVisible && isPrepared) {
            loadData(typeNO);
        }

    }


    private void loadData(String type) {
        RequestParams requestParams2 = RequstUtils.getHasTokenInstance(MURL.SALESREPORTLIST1);
        requestParams2.addBodyParameter("type", type);
        loading();
        RequstUtils.getRquest(requestParams2, mhandler, 0, 1);
    }


    private void parseJson(String result) {

        List<ProfileBean> dataList = new ArrayList<>();
        dataList.add(new ProfileBean(0, "渠道名称", "销售金额", "订单数"));

        try {
            JSONObject json = new JSONObject(result);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");
            if (ResponseCode.isSuccess(code, desc)) {

                JSONArray subReport = JsonUitls.getJSONArray(mobBaseRes, "datas");
                for (int i = 0; i < subReport.length(); i++) {

                    JSONObject item = subReport.getJSONObject(i);
                    JSONObject channelInfo = JsonUitls.getJSONObject(item, "channelInfo");
                    JSONObject salesReportBean = JsonUitls.getJSONObject(item, "salesReportBean");

                    String name = JsonUitls.getString(channelInfo, "name");
                    int order = salesReportBean.getInt("orderCount");
                    double sales = salesReportBean.getDouble("turnOver");
                    dataList.add(new ProfileBean(1, name, order, sales));

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setMyAdapter(dataList);

    }

    private void setMyAdapter(List<ProfileBean> dataList) {

        //排序
        dataList = sortArray(dataList);

        View headerView = LayoutInflater.from(getActivity()).inflate(R.layout.sales_profile_header, null);

        BarChart barChart = (BarChart) headerView.findViewById(R.id.barChart);

        //柱子没填充部分是否显示
        barChart.setDrawBarShadow(false);
        //值在柱子上/下
        barChart.setDrawValueAboveBar(true);
        //右下角图表说明文字
        barChart.getDescription().setEnabled(false);
        //是否加边框
        barChart.setDrawBorders(false);
        //超过( )个就不显示数值
        barChart.setMaxVisibleValueCount(30);
        //设置图表格子是否背景
        barChart.setDrawGridBackground(false);

        List<BarEntry> entries = new ArrayList<>();
        List<String> labels = new ArrayList<>();
        String[] lebalss = new String[5];

        int a;
        if (dataList.size() >= 6) {
            a = 6;
        } else {
            a = dataList.size();
        }
        for (int i = 1; i < a; i++) {
            entries.add(new BarEntry(i - 1, (float) dataList.get(i).getSales()));
            labels.add(dataList.get(i).getName());
            lebalss[i - 1] = dataList.get(i).getName();
        }

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new LebalSet(labels));  // 在这里设置标签
        xAxis.setEnabled(false);//设置坐标轴不显示

        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setLabelCount(6, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setEnabled(false);//设置坐标轴不显示


        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(12f);
        l.setXEntrySpace(4f);
        l.setExtra(ColorTemplate.VORDIPLOM_COLORS, lebalss);
        l.setWordWrapEnabled(true);//设置图例文字自动排列

        BarDataSet dataSet = new BarDataSet(entries, "#");
        dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);

        BarData data = new BarData(dataSet);
        barChart.setData(data);

        barChart.animateXY(0, 2000);


        pslistView.addHeaderView(headerView);
        SalesProfileAdapter adapter = new SalesProfileAdapter(getActivity(), dataList);
        pslistView.setAdapter(adapter);
    }

    //排序
    private List<ProfileBean> sortArray(List<ProfileBean> dataList) {

        for (int i = 0; i < dataList.size() - 1; i++) {
            for (int j = 1; j < dataList.size() - 1 - i; j++) {
                if (dataList.get(j).getSales() < dataList.get(j + 1).getSales()) {
                    ProfileBean temp1 = dataList.get(j);
                    ProfileBean temp2 = dataList.get(j + 1);
                    dataList.set(j, temp2);
                    dataList.set(j + 1, temp1);
                }
            }
        }
        return dataList;
    }


    //处理预加载
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            if (isVisible && isPrepared && !hasload) {
                loadData(typeNO);
            }
        } else {
            isVisible = false;

        }
    }


    @Override
    protected void initListener() {

    }

    @Override
    protected void loading() {
        sale_fg_loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        sale_fg_loading.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isdestroy = true;
    }
}
