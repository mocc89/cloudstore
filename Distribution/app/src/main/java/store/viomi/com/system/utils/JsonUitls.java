package store.viomi.com.system.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by viomi on 2016/10/25.
 * json解析封装
 */

public class JsonUitls {

    public static String getString(JSONObject json, String name) {
        String result;
        if (json != null) {
            try {
                result = json.getString(name);
            } catch (JSONException e) {
                e.printStackTrace();
                result = "null";
            }
        } else {
            result = "null";
        }
        return result;
    }


    public static JSONObject getJSONObject(JSONObject json, String name) {
        JSONObject jsonObject;

        if (json != null) {
            try {
                jsonObject = json.getJSONObject(name);
            } catch (JSONException e) {
                e.printStackTrace();
                jsonObject = new JSONObject();
            }
        } else {
            jsonObject = new JSONObject();
        }
        return jsonObject;
    }


    public static JSONArray getJSONArray(JSONObject json, String name) {
        JSONArray jsonArray;

        if (json != null) {
            try {
                jsonArray = json.getJSONArray(name);
            } catch (JSONException e) {
                e.printStackTrace();
                jsonArray = new JSONArray();
            }

        } else {
            jsonArray = new JSONArray();
        }
        return  jsonArray;
    }


}
