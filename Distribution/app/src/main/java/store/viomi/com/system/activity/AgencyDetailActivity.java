package store.viomi.com.system.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.CommonVPAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.constants.HintText;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.utils.ToastUtil;

@ContentView(R.layout.activity_agency_detail)
public class AgencyDetailActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.agency_vp)
    private ViewPager agency_vp;

    @ViewInject(R.id.name)
    private TextView nametv;

    @ViewInject(R.id.level)
    private TextView level;

    @ViewInject(R.id.status)
    private TextView statustv;

    @ViewInject(R.id.agency_logo)
    private ImageView agency_logo;

    @ViewInject(R.id.staff_num)
    private TextView staff_num;

    @ViewInject(R.id.contact_name)
    private TextView contact_name;

    @ViewInject(R.id.moblie)
    private TextView moblie;

    @ViewInject(R.id.templateName)
    private TextView template_Name;

    @ViewInject(R.id.loading)
    private RelativeLayout loading;

    private String num;
    private boolean isdestroy = false;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case 0:
                    loadingfinish();
                    String result = (String) msg.obj;
                    LogUtil.mlog("viomi", result);
                    parseJSON(result);
                    break;
                case 1:
                    loadingfinish();
                    ToastUtil.show(HintText.FAIL_REQ);
                    break;
                case 2:
                    String result2 = (String) msg.obj;
                    LogUtil.mlog("viomi2", result2);
                    if (!isdestroy) {
                        parseJSON2(result2);
                    }
                    break;

            }
        }
    };


    @Override
    protected void init() {

        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        num = intent.getStringExtra("num");

        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.CHANNELDETAIL + id + ".jaon");
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 0, 2);


        RequestParams requestParams2 = RequstUtils.getHasTokenInstance(MURL.CHANNELIMG);
        requestParams2.addBodyParameter("channelId", id);
        RequstUtils.getRquest(requestParams2, mhandler, 2, 3);

    }

    private void parseJSON(String result) {
        try {
            JSONObject json = new JSONObject(result);
            String code = JsonUitls.getString(json, "code");
            String desc = JsonUitls.getString(json, "desc");
            if (ResponseCode.isSuccess(code, desc)) {
                JSONObject resultjson = JsonUitls.getJSONObject(json, "result");

                String namestr = JsonUitls.getString(resultjson, "name");
                String channelLevel = JsonUitls.getString(resultjson, "channelLevel");

                String contactName = JsonUitls.getString(resultjson, "contactName");
                String contactMobile = JsonUitls.getString(resultjson, "contactMobile");
                String templateName = JsonUitls.getString(resultjson, "templateName");

                String status = JsonUitls.getString(resultjson, "status");
                String approveStatus = JsonUitls.getString(resultjson, "approveStatus");
                String statusDesc = JsonUitls.getString(resultjson, "statusDesc");
                String approveStatusDesc = JsonUitls.getString(resultjson, "approveStatusDesc");

                nametv.setText(namestr);
                level.setText(channelLevel + "级");

                if (!("1".equals(approveStatus) && "1".equals(status))) {
                    statustv.setTextColor(getResources().getColor(R.color.noaudit));
                } else {
                    statustv.setTextColor(getResources().getColor(R.color.detailaudit));
                }

                if ("1".equals(approveStatus)) {
                    statustv.setText(statusDesc);
                } else {
                    statustv.setText(approveStatusDesc);
                }

                staff_num.setText(num + "人");
                contact_name.setText(contactName);
                moblie.setText(contactMobile);
                template_Name.setText(templateName);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void parseJSON2(String result2) {
        try {
            JSONObject json = new JSONObject(result2);
            String code = JsonUitls.getString(json, "code");
            if ("100".equals(code)) {

                int screenWidth = getWindowManager().getDefaultDisplay().getWidth();

                DisplayMetrics dm = new DisplayMetrics();
                dm = getResources().getDisplayMetrics();
                float density = dm.density;

                int imgHight = (int) (180 * density);

                JSONObject resultjson = JsonUitls.getJSONObject(json, "result");
                String baseImg = JsonUitls.getString(resultjson, "baseImg");
                String titleImg = JsonUitls.getString(resultjson, "titleImg");
                String unionImg = JsonUitls.getString(resultjson, "unionImg");

                List<ImageView> imglist = new ArrayList<>();

                if (!"null".equals(baseImg)) {
                    ImageView iv1 = new ImageView(this);
                    iv1.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    x.image().bind(iv1, baseImg + "&thumb=1&w=" + screenWidth + "&h=" + imgHight);
                    imglist.add(iv1);
                }

                if (!"null".equals(titleImg)) {
                    ImageView iv2 = new ImageView(this);
                    iv2.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    x.image().bind(iv2, titleImg + "&thumb=1&w=" + screenWidth + "&h=" + imgHight);
                    imglist.add(iv2);
                }

                if (!"null".equals(unionImg)) {
                    ImageView iv3 = new ImageView(this);
                    iv3.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    x.image().bind(iv3, unionImg + "&thumb=1&w=" + screenWidth + "&h=" + imgHight);
                    imglist.add(iv3);
                }

                CommonVPAdapter adapter = new CommonVPAdapter(imglist);
                agency_vp.setAdapter(adapter);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void initListener() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void loading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isdestroy = true;
    }
}
