package store.viomi.com.system.constants;

/**
 * Created by viomi on 2016/10/18.
 */

public class MURL {

    public final static String BaseUrl = Config.BaseUrl;


    //版本更新检测
    public final static String UPDATE = "http://app.mi-ae.com.cn/getdata";

    //登陆接口
    public final static String LOGINURL = BaseUrl + "/channel/login/1.json";

    //我的-修改密码
    public final static String MODIFYPASSWORD = BaseUrl + "/channel/user/changePassword";

    //找回密码-发送验证码
    public final static String SENDCODE = BaseUrl + "/channel/login/sendAuthCode";
    //重置密码
    public final static String RESTPASSWORD = BaseUrl + "channel/login/recoverPassword";


    //销售报表头 --- 超级管理
    public final static String SALESREPORTHEADER = BaseUrl + "orders/analyse/channel/salesReport/eachLevel.json";

    //销售列表-直属下级 销售数据下面列表、销售概况、订单概况  -超级管理
    public final static String SALESREPORTLIST1 = BaseUrl + "channel/report/salesReport/subs.json";

    //销售数据-城市运营
    public final static String SALESREPORTLIST = BaseUrl + "channel/report/salesReport.json";

    //订单列表数据--- 超级管理
    public final static String ORDERDATALIST = BaseUrl + "fd/order/list.json";
    //订单详情--- 超级管理
    public final static String ORDERDETAIL = BaseUrl + "fd/order/";

    //订单列表数据-城市运营
    public final static String ORDERDATALISTC = BaseUrl + "orders/info/queryOrderProfitList.json";
    //订单详情--- 城市运营
    public final static String ORDERDETAILC = BaseUrl + "orders/info/queryOrderDetail/";


    //店员管理 促销员列表
    public final static String CLERKLIST = BaseUrl + "channel/user/listChannelUser";


    //经销商管理 经销商列表 门店管理 门店列表
    public final static String CHANNELLIST = BaseUrl + "channel/info/listSubChannels.json";
    //经销商详情
    public final static String CHANNELDETAIL = BaseUrl + "channel/info/queryChannelInfo/";
    //门店店头照片等图片信息
    public final static String CHANNELIMG = BaseUrl + "channel/info/queryChannelAuthAttr";


    //门店日报 列表
    public final static String STOREDAILY = BaseUrl + "channel/report/listChannelDailyReport.json";


//    public final static String


}
