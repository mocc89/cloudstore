package store.viomi.com.system.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import store.viomi.com.system.R;

public class AgencySelectActivity extends AppCompatActivity {

    private final int RESULTCODE = 1010;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agency_select);

        ImageView back = (ImageView) findViewById(R.id.back);
        final EditText name = (EditText) findViewById(R.id.name);
        Button select = (Button) findViewById(R.id.select);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameStr = name.getText().toString();
//                if (TextUtils.isEmpty(nameStr)) {
//                    ToastUtil.show("请输入经销商名称");
//                    return;
//                }
                Intent intent=new Intent();
                intent.putExtra("name", nameStr);
                setResult(RESULTCODE,intent);
                finish();
            }
        });

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.finish();
    }
}
