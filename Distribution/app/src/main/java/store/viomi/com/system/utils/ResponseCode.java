package store.viomi.com.system.utils;

/**
 * Created by viomi on 2016/10/25.
 * 返回码封装处理
 */

public class ResponseCode {

    public static boolean isSuccess(String code, String desc) {

        boolean issuccess = false;

        switch (code) {
            //处理成功
            case "100": {
                issuccess = true;
                break;
            }

            //token值为空
            case "918": {
                issuccess = false;
                ToastUtil.show(desc);
            }

            //登录已过期
            case "919": {
                issuccess = false;
                ToastUtil.show(desc);
            }

            default: {
                issuccess = false;
                ToastUtil.show(desc);
                break;
            }
        }


        return issuccess;
    }

}
