package store.viomi.com.system.bean;

/**
 * Created by viomi on 2016/11/15.
 */

public class ClerkEntity {

    private String name;
    private String channelName;
    private String roleStr;
    private String createdTime;
    private String mobile;
    private String wechatNickName;

    public ClerkEntity() {
    }

    public ClerkEntity(String name, String channelName, String roleStr, String createdTime, String mobile, String wechatNickName) {
        this.name = name;
        this.channelName = channelName;
        this.roleStr = roleStr;
        this.createdTime = createdTime;
        this.mobile = mobile;
        this.wechatNickName = wechatNickName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getRoleStr() {
        return roleStr;
    }

    public void setRoleStr(String roleStr) {
        this.roleStr = roleStr;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getWechatNickName() {
        return wechatNickName;
    }

    public void setWechatNickName(String wechatNickName) {
        this.wechatNickName = wechatNickName;
    }
}
