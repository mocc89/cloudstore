package store.viomi.com.system.constants;

/**
 * Created by viomi on 2016/11/22.
 */

public class Config {

    //当前版本号
    public final static String CURRENTVERSION = "200";
    //是否调试模式 发布false
    public final static boolean DEBUG_MODE = false;


    //开发
//    public final static String BaseUrl = "http://192.168.1.250/services/";
    //测试
    public final static String BaseUrl = "http://vj.viomi.com.cn/services/";
    //生产
//    public final static String BaseUrl = "http://s.viomi.com.cn/services/";
}
