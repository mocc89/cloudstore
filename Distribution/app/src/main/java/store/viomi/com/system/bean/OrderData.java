package store.viomi.com.system.bean;

/**
 * Created by viomi on 2016/11/8.
 */

public class OrderData {

    private String orderCode;
    private String linkmanName;
    private String linkmanPhone;
    private String createdTime;
    private String price;

    public OrderData() {
    }

    public OrderData(String orderCode, String linkmanName, String linkmanPhone, String createdTime, String price) {
        this.orderCode = orderCode;
        this.linkmanName = linkmanName;
        this.linkmanPhone = linkmanPhone;
        this.createdTime = createdTime;
        this.price = price;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getLinkmanName() {
        return linkmanName;
    }

    public void setLinkmanName(String linkmanName) {
        this.linkmanName = linkmanName;
    }

    public String getLinkmanPhone() {
        return linkmanPhone;
    }

    public void setLinkmanPhone(String linkmanPhone) {
        this.linkmanPhone = linkmanPhone;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
