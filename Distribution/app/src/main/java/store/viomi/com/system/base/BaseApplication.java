package store.viomi.com.system.base;

import android.app.Application;

import org.xutils.BuildConfig;
import org.xutils.x;

/**
 * Created by viomi on 2016/10/18.
 */

public class BaseApplication extends Application {

    private static String user_name;
    private static String channel_name;
    private static String token;
    private static BaseApplication app;

    private static String downlink;
    private static String appversion;


    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        //初始化xutils
        x.Ext.init(this);
        x.Ext.setDebug(BuildConfig.DEBUG);
    }


    public static String getChannel_name() {
        return channel_name;
    }

    public static void setChannel_name(String channel_name) {
        BaseApplication.channel_name = channel_name;
    }

    public static String getUser_name() {
        return user_name;
    }

    public static void setUser_name(String user_name) {
        BaseApplication.user_name = user_name;
    }


    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        BaseApplication.token = token;
    }

    public static BaseApplication getApp() {
        return app;
    }

    public static String getDownlink() {
        return downlink;
    }

    public static void setDownlink(String downlink) {
        BaseApplication.downlink = downlink;
    }

    public static String getAppversion() {
        return appversion;
    }

    public static void setAppversion(String appversion) {
        BaseApplication.appversion = appversion;
    }
}
