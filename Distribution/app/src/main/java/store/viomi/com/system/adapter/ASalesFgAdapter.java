package store.viomi.com.system.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by viomi on 2016/10/27.
 */

public class ASalesFgAdapter extends FragmentPagerAdapter {

    private List<Fragment> list;
    private String[] titles;

    public ASalesFgAdapter(FragmentManager fm, List<Fragment> list, String[] titles) {
        super(fm);
        this.list = list;
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    //获得该页 的标题
    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

}
