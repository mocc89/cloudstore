package store.viomi.com.system.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by viomi on 2016/11/23.
 */

public class CommonVPAdapter extends PagerAdapter {

    private List<ImageView> list;

    public CommonVPAdapter(List<ImageView> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    //添加ImageView到ViewPager中
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        //根据position将List中的ImageView添加到ViewPager中
        container.addView(list.get(position));
        //将添加的ImageView返回
        return list.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //从容器中移除一个ImageView
        container.removeView(list.get(position));
    }
}
