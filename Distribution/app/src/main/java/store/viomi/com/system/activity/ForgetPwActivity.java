package store.viomi.com.system.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import store.viomi.com.system.R;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.constants.HintText;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.utils.ToastUtil;

@ContentView(R.layout.activity_forget_pw)
public class ForgetPwActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.input_account)
    private EditText input_account;

    @ViewInject(R.id.send_code)
    private Button send_code;

    @ViewInject(R.id.loading_bg)
    private RelativeLayout loading_bg;

    private final static int REQUESTCODE = 1003;
    private final static int RESPONSECODE = 1004;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case 0: {
                    loadingfinish();
                    String result = (String) msg.obj;
                    parseJson(result);
                    break;
                }
                case 1: {
                    loadingfinish();
                    ToastUtil.show(HintText.FAIL_REQ);
                    break;
                }
            }
        }
    };


    @Override
    protected void init() {

    }


    private void sendCode() {
        String accountNum = input_account.getText().toString();

        if (accountNum.isEmpty()) {
            ToastUtil.show(HintText.CANNOTEMPTY);
            return;
        }

        RequestParams requestParams = RequstUtils.getNoTokenInstance(MURL.SENDCODE);
        requestParams.addBodyParameter("mobile", accountNum);

        loading();

        RequstUtils.postRquest(requestParams, mhandler, 0, 1);

    }

    private void parseJson(String result) {

        try {
            JSONObject json = new JSONObject(result);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");
            if (ResponseCode.isSuccess(code, desc)) {
                Intent intent = new Intent(this, ForgetPwCodeActivity.class);
                String phone_num = input_account.getText().toString();
                intent.putExtra("phone_num", phone_num);
                startActivityForResult(intent,REQUESTCODE );
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void initListener() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        send_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCode();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==REQUESTCODE&&resultCode==RESPONSECODE) {
            finish();
        }
    }

    @Override
    protected void loading() {
        loading_bg.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading_bg.setVisibility(View.GONE);
    }
}
