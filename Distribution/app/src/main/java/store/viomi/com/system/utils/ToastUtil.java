package store.viomi.com.system.utils;

import android.widget.Toast;

import store.viomi.com.system.base.BaseApplication;

/**
 * Created by viomi on 2016/10/24.
 * toast封装
 */

public class ToastUtil {

    public static void show(String result) {
        Toast.makeText(BaseApplication.getApp(), result, Toast.LENGTH_SHORT).show();
    }
}
