package store.viomi.com.system.utils;

import android.util.Log;

import store.viomi.com.system.constants.Config;

/**
 * Created by viomi on 2016/11/16.
 */

public class LogUtil {

    public static void mlog(String tag,String logtxt){
        if (Config.DEBUG_MODE) {
            Log.e(tag, logtxt);
        }
    }
}
